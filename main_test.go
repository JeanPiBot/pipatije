package main

import (
	"fmt"
	"testing"
)

func TestAleatorio(t *testing.T) {
	max := 3
	min := 1

	arrayOutput := []int{1,2,3}
	var expectedOutput int 

	output := Aleatorio(max, min)

	for i := 0; i < len(arrayOutput); i++ {
		if arrayOutput[i] == output {
			expectedOutput = arrayOutput[i]
		}
	}

	if expectedOutput != output {
		t.Errorf("Failed ! got %v want %c", output, expectedOutput)
	} else {
		t.Logf("Success !")
	}
}

func TestEleccion(t *testing.T) {
	tests := []struct{
		input int
		output string
	}{
		{1, "Elegiste  🪨"},
		{2, "Elegiste 📝"},
		{3, "Elegiste ✂️"},
		{4, "No elegiste un número"},
		{5, "No elegiste un número"},
		{1000, "No elegiste un número"},
		{-1, "No elegiste un número"},
		{-1000, "No elegiste un número"},
		{0, "No elegiste un número"},
		{100, "No elegiste un número"},
	}

	for i, tc := range tests {
		t.Run(fmt.Sprintf("Eleccion=%d", i), func(t *testing.T) {
			got := Eleccion(tc.input)
			if got != tc.output {
				t.Fatalf("got %v; output %v", got, tc.output)
			} else {
				t.Logf("Success !")
			}

		})
	}
}