package main

import (
	"fmt"
	"math/rand"
	"time"
)

func Aleatorio(maximo, minimo int) int {
	return rand.Intn(maximo - minimo) + minimo
}

func Eleccion(jugada int) string {
	var resultado string = ""
	if jugada == 1 {
		resultado = "Elegiste  🪨"
	} else if jugada == 2 {
		resultado = "Elegiste 📝"
	} else if jugada == 3 {
		resultado = "Elegiste ✂️"
	} else {
		resultado = "No elegiste un número"
	}

	return resultado
}

func main() {

	rand.Seed(time.Now().UnixNano())

	var min int = 1
	var max int = 3
	var triunfos int = 0
	var perdidas int = 0
	var pc int = 0
	var jugador int = 0
	
	for triunfos < 3 && perdidas < 3 {
		pc = Aleatorio(max, min)
		fmt.Println("Elige: 1 para 🪨, 2 para 📝 y 3 para ✂️")
		fmt.Scanln(&jugador)

		fmt.Println("Pc elige: " + Eleccion(pc))
		fmt.Println("Tu elegiste: " + Eleccion(jugador))

		if pc == jugador {
			fmt.Println("Empate")
		} else if jugador == 1 && pc == 3 || jugador == 2 && pc == 1 || jugador == 3 && pc == 2 {
			fmt.Println("Ganaste")
			triunfos++
		} else {
			fmt.Println("Perdiste")
			perdidas--
		}
	}

	fmt.Printf("Ganaste: %d veces y perdiste: %d veces", triunfos, perdidas)
}